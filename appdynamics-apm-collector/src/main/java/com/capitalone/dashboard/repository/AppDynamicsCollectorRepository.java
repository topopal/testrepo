package com.capitalone.dashboard.repository;

import com.capitalone.dashboard.model.AppDynamicsCollector;

public interface AppDynamicsCollectorRepository extends BaseCollectorRepository<AppDynamicsCollector> {
}
