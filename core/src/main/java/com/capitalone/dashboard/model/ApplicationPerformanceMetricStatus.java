package com.capitalone.dashboard.model;

/**
 * Enumerates the possible {@link ApplicationPerformanceMetric} statuses.
 */
public enum ApplicationPerformanceMetricStatus {
    Ok, Warning, Alert, Error
}
